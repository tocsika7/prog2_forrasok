import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class LZWBinFaTest {
    LZWBinFa binfa = new LZWBinFa();

    @Test
    public void BinfaTest(){
        
        String input ="01111001001001000111";
 

        for(char input_char : input.toCharArray()){
            binfa.egyBitFeldolg(input_char);
        }

        assertEquals(4,binfa.getMelyseg(),0.0);
        assertEquals(0.957427,binfa.getSzoras(),0.0001);
    }
}

