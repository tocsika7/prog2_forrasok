class Madar{
	public void repul(){};

	static class Program{
		public void fgv(Madar madar){
			madar.repul();
		}
	}

	static class Sas extends Madar{}
	static class Pingvin extends Madar{}

	public static void main(String[] args) {
	
		Program program = new Program();
		Madar madar = new Madar();
		program.fgv(madar);

		Sas sas = new Sas();
		program.fgv(sas);

		Pingvin pingvin = new Pingvin();
		program.fgv(pingvin);
	}

}