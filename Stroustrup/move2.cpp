#include <iostream>
#include <string.h>

class String{
    private:
        char* m_Buffer; //Buffer
        unsigned int m_Size; //Meret 
    public:
        String(const char* string)
        {
            m_Size = strlen(string);    //Meghatározzzuk a string hosszát
            m_Buffer = new char[m_Size + 1];    //Buffer létrehozása a szükséges mérettel
            memcpy(m_Buffer, string, m_Size);   //Bemásoljuk a stringet a buffrba   
            m_Buffer[m_Size] = 0;
        }

      
        ~String(){      //Destruktor
            delete[] m_Buffer;
        }

        char& operator[](unsigned int index)        //Index operátor 
        {
            return m_Buffer[index];
        }
        
        String(String&& other){         //A move függvénnyel készítünk belőle rvalue-t az objektumra this mutatóját az új objektumra állítjuk az eredetit kinullázzuk
            std::cout<<"Move ctor\n";
            m_Buffer = other.m_Buffer;
            other.m_Buffer = nullptr;
        }
        
    /*    String& operator=(String other){          
            std::swap(m_Buffer,other.m_Buffer); //swappal cseréljük a két buffert, ami mögött a move dolgozik a this mutatóval visszadjuk az új objektumot. 
            return *this;
        }*/

        String& operator=(String&& other){
            if(this != &other){
                delete m_Buffer;

                m_Buffer = other.m_Buffer;
                other = nullptr;
            }      
            return *this;
        }


    friend std::ostream& operator<<(std::ostream& stream, const String& string);    //Kiíratás operátor túlterheléssel
        
};

std::ostream& operator<<(std::ostream& stream, const String& string){
        stream << string.m_Buffer;
        return stream;
    }


int main(){
    String string1 = "test";
    std::cout<<"Az eredeti string: "<<string1<<std::endl;
    String string2 = std::move(string1);
    std::cout<<"A másolt string: "<<string2<<std::endl;
    std::cout<<"Az eredeti string másolás után:  "<<string1<<std::endl;



  
}

