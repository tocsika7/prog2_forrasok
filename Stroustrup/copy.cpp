#include <iostream>
#include <string.h>

class String{
    
      
    public:
        char* m_Buffer; //Buffer
        unsigned int m_Size; //Meret 
        String(const char* string)
        {

            m_Size = strlen(string);    //Meghatározzzuk a string hosszát
            m_Buffer = new char[m_Size + 1];    //Buffer létrehozása a szükséges mérettel
            memcpy(m_Buffer, string, m_Size);   //Bemásoljuk a stringet a buffrba   
            m_Buffer[m_Size] = 0;
        }

       String(const String& other)
        : m_Size(other.m_Size)  //A méret int nem kell deep copy
        {
            std::cout<<"Copy ctor"<<std::endl;
            m_Buffer = new char[m_Size + 1]; //allokálunk egy új buffert 
            memcpy(m_Buffer,other.m_Buffer,m_Size+1); //a buffert belemásoljuk az új bufferbe 
        }

        ~String(){      //Destruktor
            delete[] m_Buffer;
        }

        char& operator[](unsigned int index)        //Index operátor 
        {
            return m_Buffer[index];
        }


    friend std::ostream& operator<<(std::ostream& stream, const String& string);    //Kiíratás operátor túlterheléssel
        
};

std::ostream& operator<<(std::ostream& stream, const String& string){
        stream << string.m_Buffer;
        return stream;
    }


int main(){
    String string = "test";        //Lemásoljuk a stringet, ilyenkor a C++ alap másoló konstruktora hívódik meg, ami csak egy felszíni másolást végez a két objektumnak char* tömbjének meg fog egyezni a memóriacíme és hibát kapunk. 
    String string2 = string;
    string2[2]='b';        //Ilyenkor, olyan hiba is keletkezhet, hogyha megváltoztatjuk a másolt objektumot az eredeti is változik. 
    
   

    std::cout<< string<< std::endl;
    std::cout<< string2 << std::endl;

  
}

