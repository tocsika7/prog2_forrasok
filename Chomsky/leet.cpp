#include <iostream>
#include <fstream>
#include <string.h>
#include <stdio.h>

class leet
{
private:
    std::string leetchars[26]{
        "4", "8", "<", "[)", "3", "|=", "6", "|-|", "1", "_|", "|<", "|", "|V|", "|\\|", "O",
        "|>", "0.", "|2", "5", "7", "|_|", "\\/", "\\X/", "}{", "`/", "2"};

    std::string leetnums[10]{
        "O", "I", "Z", "E", "A", "S", "G", "T", "B", "g"};

public:
    std::string cipher(int ch)
    {
        if (ch >= 65 && ch <= 90)
        {
            return leetchars[ch - 65];
        }
        else if (ch >= 48 && ch <= 57)
        {
            return leetnums[ch - 48];
        }
        else
        {
            return std::to_string(ch);
        }
    }
};

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "Használat: ./leet input.txt output.txt \n";
        return -1;
    }

    std::ifstream befile(argv[1]);
    std::ofstream kifile(argv[2]);

    leet l;
    std::string temp;

    while (!befile.eof())
    {
        befile >> temp;
    }

    char output[temp.length() + 1];
    strcpy(output, temp.c_str());

    for (int i = 0; i < temp.length(); i++)
    {
        kifile << (l.cipher((int)(toupper(output[i]))));
    }

    befile.close();
    kifile.close();

    return 0;
}